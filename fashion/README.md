## [Fashion Example](https://www.tensorflow.org/tfx/tutorials/serving/rest_simple) using kubeflow on ai-platform 
This playbook shows an example of fashion object categorization. The main purpose of this work is to demonstrate an end-to-end example, including training, testing, inferencing, and restful-api services. 

As the earlier xor example, we also demonstrate two different implementations. Jupyter folder includes end-to-end process on jupyter notebook alone; while pipeline shows a re-usable method to wrap things into container/component.yaml. kfserving folder shows the yaml code which could quickly serve the model by using kfserving framework. 

In this tutorial, I encourage audience to read samples at jupyter/basic_classification.ipynb to understand how to provide payload to test the fashion services. We'll further utilize kfserving's pre/post processing stage to eliminate the process in the future release.
