## Objective:

XOR is a simple exclusive-or result estimator. This tutorial is trying to utilize keras training framework on this simple question. The output is a score between 0 to 1. The result is a result of a rounding-up.

The work has 2 different implementations. The first one is in jupyter folder, where all the data loadings, trainings, and kf-serving are all implemented/deployed by jupyter notebook. The second implementation is using the kfp SDK to wrap the implementation into header files and container images. Its serving uses yaml file instead of jupyter. 

Once the service is deployed with kfserving, it became a restful API service and  predict.sh demonstrates a quick way to test the service. 

```bash
jupyter@myaibook:~/mlops-anthos/xor/jupyter$ ./predict.sh 
* Expire in 0 ms for 6 (transfer 0x5647d8ed5f90)
*   Trying 104.155.215.124...
* TCP_NODELAY set
* Expire in 200 ms for 4 (transfer 0x5647d8ed5f90)
* Connected to 104.155.215.124 (104.155.215.124) port 80 (#0)
> POST /v1/models/xor-simple:predict HTTP/1.1
> Host: xor-simple.default.example.com
> User-Agent: curl/7.64.0
> Accept: */*
> Content-Length: 24
> Content-Type: application/x-www-form-urlencoded
> 
* upload completely sent off: 24 out of 24 bytes
< HTTP/1.1 200 OK
< content-length: 43
< content-type: application/json
< date: Thu, 21 Jan 2021 23:39:20 GMT
< x-envoy-upstream-service-time: 7
< server: istio-envoy
< 
{
    "predictions": [[0.538382709]
    ]
* Connection #0 to host 104.155.215.124 left intact
}
```
The tutorial should be self-explanable.  
