#!/bin/bash
test=$1
kubeflow_url="104.155.215.124"
MODEL_NAME=xor-jupyter
SERVICE_HOSTNAME=$(kubectl get inferenceservice ${MODEL_NAME} -o jsonpath='{.status.url}' | cut -d "/" -f 3)
curl -v -H "Host: ${SERVICE_HOSTNAME}" http://${kubeflow_url}/v1/models/${MODEL_NAME}:predict -d '{"instances": [ [1,1] ]}';echo
