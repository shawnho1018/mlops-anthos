# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
import os
from pathlib import Path
import tensorflow as tf
import keras
from google.cloud import storage
import numpy as np

def read_gcs(filename: str, colsize: int):
    client = storage.Client()
    bucket = client.get_bucket('tsmc-demo')
    blob = bucket.get_blob(filename)
    data = blob.download_as_string()
    rows = data.split(b"\n")
    offset = max([len(i) for i in rows])
    print('offset:', offset)
    for i in rows:
        words=i.split(b' ')
        print('debug:', words)

    matrix = np.empty((0,colsize), int)
    for i in rows:
        if len(i) >= offset:
            words=i.split(b' ')
            relevant_row = np.array(words, ndmin=2)
            #relevant_row = relevant_row[np.where(relevant_row != b'')]  #gets rid of all the trailing commas at the end
            relevant_row = relevant_row.astype(int)
            matrix=np.append(matrix, relevant_row, axis=0)
    return matrix;

def read_model(filename: str):
    client = storage.Client()
    bucket = client.get_bucket('tsmc-demo')
    blob = bucket.get_blob(filename)
    model_json_string = blob.download_as_string()
    print(model_json_string)
    model = keras.models.model_from_json(model_json_string)
    return model


parser = argparse.ArgumentParser(description='Train classifier model using Keras')

parser.add_argument('--training-set-features-path', type=str, help='Local or GCS path to the training set features table.')
parser.add_argument('--training-set-labels-path', type=str, help='Local or GCS path to the training set labels (each label is a class index from 0 to num-classes - 1).')
parser.add_argument('--output-model-path', type=str, help='Local or GCS path specifying where to save the trained model. The model (topology + weights + optimizer state) is saved in HDF5 format and can be loaded back by calling keras.models.load_model')
parser.add_argument('--model-config-json', type=str, help='JSON string containing the serialized model structure. Can be obtained by calling model.to_json() on a Keras model.')
parser.add_argument('--num-classes', type=int, help='Number of classifier classes.')
parser.add_argument('--num-epochs', type=int, default=100, help='Number of epochs to train the model. An epoch is an iteration over the entire `x` and `y` data provided.')
parser.add_argument('--batch-size', type=int, default=32, help='Number of samples per gradient update.')

args = parser.parse_args()

# The data, split between train and test sets:
#(x_train, y_train), (x_test, y_test) = cifar10.load_data()
print('training_features:', args.training_set_features_path)
print('training_labels:', args.training_set_labels_path)
x_train = read_gcs(args.training_set_features_path, 2)
y_train = read_gcs(args.training_set_labels_path, 1)
print('train samples:', x_train)
print('label samples:', y_train)

model = read_model(args.model_config_json)

# Let's train the model using RMSprop
model.compile(loss='mean_squared_error',
              optimizer='adam',
              metrics=['binary_accuracy'])

model.fit(
    x_train,
    y_train,
    batch_size=args.batch_size,
    epochs=args.num_epochs,
    shuffle=True
)

# Save model and weights
model.save(args.output_model_path)
print('Saved trained model at %s ' % args.output_model_path)