# mlops-anthos
This project documents my own learning experience using Kubeflow framework. It includes 4 different parts:

author: shawnho@google.com
## install-tutorial  
This folder contains kubernetes cluster installation and kfserving. The only thing you need to do in advance is to set your own project before running the scripts.
```
gcloud config set project ${your-own-project-id}
```
You could also try to modify the cluster name (index). The expected result of the script should be a GKE cluster in asia-east1-a zone with kfserving deployed. 
## data-process  
This folder mainly consists of 3 entry levels examples which helps audience being familiar with kubeflow framework and its integration with surrouding infrastructure, such as Google Cloud Storage, Google Container Registry, and BigQuery service.
* lightweight_component.ipynb: Get familiar with containerization process
* data.ipynb: Get familiar with GCS input/output and pipeline design.
* ai_platform.ipynb: Get familiar with Google BigQuery service.
For details, please refer to [this link](https://gitlab.com/shawnho1018/mlops-anthos/-/tree/master/data-process)
## xor
This folder demonstrates two end-to-end implementations for xor operator prediction. The reason we choose this is its simplicity of producing training data. For details, please refer to [this link](https://gitlab.com/shawnho1018/mlops-anthos/-/tree/master/fashion) 
## fashion  
This folder shows two end-to-end implementations for fashion product categorization. The training/test data is available from keras.datasets. We choose this sample is to show case how to process two-dimensional input data, such as image. We include the details in [this link](https://gitlab.com/shawnho1018/mlops-anthos/-/tree/master/fashion)

