# Copyright 2020 Google, LLC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START eventarc_gcs_server]
import os
import requests
import base64
import json
import logging
from google.cloud import storage
from flask import Flask, request


app = Flask(__name__)
# [END eventarc_gcs_server]

def download_gcs_file(bucket_name, source_blob_name, destination_file_name):
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)

    # Construct a client side representation of a blob.
    # Note `Bucket.blob` differs from `Bucket.get_blob` as it doesn't retrieve
    # any content from Google Cloud Storage. As we don't need additional data,
    # using `Bucket.blob` is preferred here.
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)

    print(
        "Blob {} downloaded to {}.".format(
            source_blob_name, destination_file_name
        )
    )    

# [START eventarc_gcs_handler]
@app.route('/', methods=['POST'])
def index():
    # Gets the GCS bucket name from the CloudEvent header
    # Example: "storage.googleapis.com/projects/_/buckets/my-bucket"
    url = os.environ.get('service_url')
    print(f"parsed url: {url}")
    bucket = os.environ.get('bucket')
    print(f"parsed bucket: {bucket}")
    blob = request.headers.get('ce-subject').lstrip("objects/")
    print(f"new version 1.0")
    print(f"Detected change in Cloud Storage blob: {blob} and run result")
    download_gcs_file(bucket, blob, "./image.png")
    try: 
        image = open("./image.png", 'rb') #open binary file in read mode
        image_read = image.read()
        image_64_encode = base64.b64encode(image_read)
        bytes_array = image_64_encode.decode('utf-8')
        payload = {
            "instances":[
                {
                    "data": bytes_array
                }
            ]
        }
        r = requests.post(url, json=payload)
        print(r.json())        
    except FileNotFoundError:
        print(f"file: {bucket} does not exist in bucket")
 
    return (f"", 200)
# [END eventarc_gcs_handler]


# [START eventarc_gcs_server]
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
# [END eventarc_gcs_server]
