# Cloud Storage via Cloud Run on Anthos tutorial

This sample shows how to create a service that processes GCS using 
[the CloudEvents SDK](https://github.com/cloudevents/sdk-python). It demonstrates the following:
* Receive trigger message from GCS file upload event 
* Read file from GCS and storage locally
* Send the image to Inferencing Server to receive the identified result.
* Ack the event to prevent trigger re-occurences.

See tutorial, [Quickstart: Receiving Cloud Storage events](https://cloud.google.com/eventarc/docs/run/quickstart?hl=en#python), 
tutorial for instructions for setting up and deploying this sample application.
