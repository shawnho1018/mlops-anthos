#!/bin/bash
export PROJECT_ID="shawn-demo-2021"
export PROJECT_NUMBER="864621875982"

# create event broker
kubectl create namespace events
gcloud beta events namespaces init events \
  --copy-default-secret
gcloud beta events brokers create default \
  --namespace events
kubectl get brokers \
  -n events

# create cloudrun 
docker build -t gcr.io/${PROJECT_ID}/mlops-events ./
docker push gcr.io/shawn-demo-2021/mlops-events
gcloud run deploy mlops-events \
    --namespace events \
    --image gcr.io/$(gcloud config get-value project)/mlops-events

# allow cloud storage to publish events to pub/sub
serviceaccount=$(curl -X GET -H "Authorization: Bearer $(gcloud auth print-access-token)" \
  "https://storage.googleapis.com/storage/v1/projects/$(gcloud config get-value project)/serviceAccount" | jq '.email_address')
echo $serviceaccount
gcloud projects add-iam-policy-binding $(gcloud config get-value project) \
    --member=serviceAccount:${serviceaccount} \
    --role roles/pubsub.publisher
gcloud iam service-accounts add-iam-policy-binding cloud-storage --member serviceAccount:${serviceaccount} --role roles/pubsub.publisher


# create cloud storage triggers
gcloud beta events triggers create trigger-storage-1 \
  --namespace events \
  --target-service mlops-events \
  --type=google.cloud.storage.object.v1.finalized \
  --parameters bucket=mlops-images-shawn-demo-2021
