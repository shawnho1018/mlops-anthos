#!/bin/bash
test_file=$1
kubeflow_url="127.0.0.1"
MODEL_NAME=captcha5-custom
echo "convert file ${test_file} into input.json"
python img2bytearray.py ${test_file}
curl -v -X POST -H "Content-Type: application/json" http://${kubeflow_url}:5500/predictions/${MODEL_NAME} -d @input.json; echo
