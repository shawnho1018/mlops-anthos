#!/bin/bash
if [ $# -eq 0 ]; then
	echo "No input file"
	test_file="captcha5_1000/SCN_202007311458_00036.png"
else
	test_file=$1
fi
kubeflow_url=$(kubectl get svc -n istio-system | grep istio-ingressgateway | awk '{print $4}')
MODEL_NAME=captcha5-custom
kubeflow_url="demo.default.shawnk8s.com"
echo "convert file ${test_file} into input.json"
python img2bytearray.py ${test_file}
curl -v -X POST -H "Content-Type: application/json" http://${kubeflow_url}/predictions/${MODEL_NAME} -d @input.json; echo
