# Captcha5 Identification with TorchServe Framework
## Maintenancer: shawnho@google.com
## TL;DR
This project demonstrates how to use torchserve framework to write an inference service with a set of pre-trained parameters. Gitlab-CI is also included to show an self-service model to validate the implementation. 

### TorchServe Framework Introduction
This work demonstrates how to use [torchserve framework](https://github.com/pytorch/serve) to serve a trained model. Its serving architecture shows below ![torchserve](images/torchserve-diagram.jpg)


There are two subfolders in this project:
* test: This folder contains a test code to validate the deployed service. Replace kubeflow_url in test/predict-k8s.sh accordingly and a sample image is provided for a quick test.
```
test/predict-k8s.sh test/SCN_202007311458_00036.png 
```
predict-k8s.sh would first convert the image into a bytearray and then use this bytearray as http payload.

* platform: This folder also contains the source code and dockerfile. 

We hope to show readers the following:
1. How to write handler function to handle daily images?

Torchserve's handler functions are mainly divided as the following components:
* Initialize
  Load background_d.pkl, model parameters (conv_ae_013399.pth), and model (class AutoEncoder) into the server.

* Preprocess 
  Parse incoming http payload, rectify image, and segment raw image into sub-image which contains one word.
  The most difficult part in preprocess was the image parser. If kfserving default image could be applied, there are a default parser, which could process payload with the following structure:
  ```
  {
    "instances": [
        {
            "data": "iVBORw0KGgoAAAANSUhEUgAAAEYAAAAZCAYAAACM9limAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAPtSURBVFhH7VfRjR0hDHz95HNrSQtbx7WxLVwt7/fqeZkxHjDYe0K6KIpyCfIBg23sgTUvD/x7rXK+Hd8egyTgdb7/xyAZ/BMn8rdjkASAwcL4m2GQBfjxeF1sH/Z3tDA/fjZH59ti+w9hD14jfmPWQy6wZ+l/nK/Tes4wEjHoybCJ2bJ3+4hB3MJa8w+9ye5z7PV+QZ4mbW3VLfa9w9Bf75ZRbyfmd7YgJ7Mlo4g1583x8UM3priW0LOeuiTTCb3Tu8Oeb4OUC+OBnx1f5XCdE/GtazxoO1RG42M1J2KVBNQJk3FvA5t1GnYYcb5lv3WV3h12/UTyH54Uk/FPl9IJs3WMpYcxy4DZizzYsn9GEkJuHBsKooQFScBtwkzwWBxHnYaNGtVvDVrWq2yxB0lhMiDg6Z/S8Zh1FZ9IInEilbqyM1scUrzp64HcxQdJQBm0XT93Lqw6denZp6hCjrbqVZ8Sa4CRwtPH3JLDjVj1FJ9uy4F9iJEgEcJbo/hETLMNvmJ8IS8TcxiKbx/DKZ30sTNrWFzr86anEzAszruv4DP0LPoipZqvdlqncB5vCde6/xhnsGcfa80aH8gJLLn4Qhc+z814KchwPs/9VrkeT7Jt+/mN6cUSN6AXUP+kmKT0uj6T8PVRYzhGjMCHXm1rvcfKtubhkoCcMBIlu/r9MvAxF3lsHeMV9xMRJtEe1FFicV/dgJIYYK2++C3BHvJB6a8mTr+yVZxsVncKAiEJ6KxK5CRilKhnGk5Cao6ThGHbgmmJjaQq0S0atoNM1aP4aQm7u/kWD0SvWEUgJIPx5MgonZ3Fkxb1tKE90CKIcy/GbLHAMeh4W9Lpe8+nVjbdViSEwkx/VnydZH6eUx4kxeOyh8Fx2ca5iTlUYSvG45VxjN93HJteGMceuIVCgqKd9pAd+7Dv018nvlJDb/RaJ5mrT+GtaDffyqHnsdj0eYgR5CxMQXzBZBTezCqdlJjfMmt+SmzTU48goh3F6kr6rPKN6cQVhTb+OCTW41ccIZ44lp8gCTAmE1YkUpIFPX1+dlPab18b65s2vYJUEWE99NmrXlDifxNWAlWk4wtnNwc9o2gk+O3X2Ho09LH+uSSgTri6HRVZu3rlHvN8xWLCbTzI6/6sbg0C4/NN2Y0ZkoA66N9NVmn7BazcY54bVvqb5yZUpFMuTuOpJ671oXfJZsEHxj7iGPd14Xc+gm6ps+jaerTxPuGL3arvPcjJbF1tYRJX3sDmecOyXv2L9gtY6W+eN2xPD5LBapP6F+g8b9hu0Jt6u3ts+9vDIAm4YbrCNgPc9ldhX/G3a7tNzGbCuye8a7u77++OJZH1eP0Cj8uQrmgqizAAAAAASUVORK5CYII="
        }
    ]
  }
  ```

  Since custom image is applied in our case, a custom parser needs to be provided to parse the packet payload:
  ```
  # we only get the first image from the array (if there is any) to process.
  preprocessed_data = data[0].get("data")
  if preprocessed_data is None:
    print('frontend does not parse body for us')
    preprocessed_data = data[0].get("body").get("instances")
    print('shawn-preprocess image count:', len(preprocessed_data))
    if len(preprocessed_data) > 1:
        print("shawn-Image count is more than 1...Only Process the first one and skip the rest")
    enc_image = preprocessed_data[0].get("data")
  ```
  
  image_segmentation function further removes the background, divides, and normalizes the captcha5 image into an image array (one character for each image) for the next stage.
  ![image-array](./images/image-array.png)  

* Inference
  Comparing each image in the array with 0-9,A-Z to get the highest matching score. Output the character which has the maximal scores.

* Postprocess 
  Torchserve requires the output as list with size of 1. Postprocess converts the identified string into a string list. 

2. How to use "torch-model-archiver"  to combine the model parameter (*.pkl) with background image and handler file to create model file (*.mar). 
  All 3 modules above were programmed in handler-captcha5.py file. However, we could not directly utilize serverless (a.k.a Cloudrun) for this pytorch model because some python libraries in img_prepro.py was not included. We therefore apply custom docker image to include those libraries to serve. 
  
  torch-model-archiver function was also applied to package the handler, model parameters, and extra-files (e.g. background.pkl and preprocess python code) into a torchserve model (.mar file). The details could refer to the torchserve.dockerfile. 
