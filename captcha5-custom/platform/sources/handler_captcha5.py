import logging
import numpy as np
import os
import torch
import uuid
import pickle
import base64
from ts.torch_handler.base_handler import BaseHandler
from model import autoencoder
from img_prepro import img_segmentation
from utils import get_key

logger = logging.getLogger(__name__)

class captcha5_identifier(BaseHandler): 
    def __init__(self):
        self._context = None
        self.initialized = False
        self.explain = False
        self.captcha_model = None
        self.background = None
        self.key = None

    def initialize(self, context):
        """
        Initialize model. This will be called during model loading time
        :param context: Initial context contains model server system properties.
        :return:
        """
        self._context = context
        self.initialized = True
        self.key = get_key()
        
        # Load pwd 
        properties = context.system_properties
        model_dir = properties.get("model_dir")
        print("shawn-pwd:", model_dir)
        
        # Read model serialize/pt file
        self.manifest = context.manifest
        serialized_file = self.manifest['model']['serializedFile']         
        model_pt_path = os.path.join(model_dir, serialized_file)
        if not os.path.isfile(model_pt_path):
            raise RuntimeError("Missing the model.pth file")        
        stat_dict = torch.load(model_pt_path, map_location=torch.device('cpu'))
        
        # Load Background pkl file
        background_path = os.path.join(model_dir, "background_d.pkl")
        if not os.path.isfile(background_path):
            raise RuntimeError("Missing the model.pth file")       
        with open(background_path, "rb") as f:
            print('shawn-loading background image:', background_path)
            self.background = pickle.load(f)
            
        #  load the model, refer 'custom handler class' above for details
        self.captcha_model = autoencoder(n_class=len(self.key))
        print('shawn-load-trained-parameter')
        self.captcha_model.load_state_dict(stat_dict)
        
    def preprocess(self, data):
        """
        Transform raw input into model input data.
        :param batch: list of raw requests, should match batch size
        :return: list of preprocessed model input data
        """
        # Take the input data and make it inference ready
        preprocessed_data = data[0].get("data")
        if preprocessed_data is None:
            print('frontend does not parse body for us')
            preprocessed_data = data[0].get("body").get("instances")
            print('shawn-preprocess image count:', len(preprocessed_data))
            if len(preprocessed_data) > 1:
                print("shawn-Image count is more than 1...Only Process the first one and skip the rest")
            enc_image = preprocessed_data[0].get("data")
        else:
            print('No instances')
            enc_image = preprocessed_data
        if isinstance(enc_image, str):
            # if the image is a string of bytesarray.
            print('shawn-image is string type')

        rectify_imgs = img_segmentation(enc_image, self.background)
        stackedimg = torch.stack([ torch.FloatTensor(img) for img in rectify_imgs ], 0)
        imgs = stackedimg.unsqueeze(1)#.cuda()        
        return imgs

    def inference(self, model_input):
        """
        Internal inference methods
        :param model_input: transformed model input data
        :return: list of inference output in NDArray
        """
        # Do some inference call to engine here and return output
        print('shawn-inference')
        [rc_imgs, pred_labels] = self.captcha_model.forward(model_input)
        pred_labels = pred_labels.cpu().data.numpy()
        model_output = np.argmax(pred_labels, 1)
        print('shawn-inference-result:', model_output)
        return model_output

    def postprocess(self, inference_output):
        """
        Return inference result.
        :param inference_output: list of inference output
        :return: list of predict results
        """
        # Take output from network and post-process to desired format
        print('shawn-postprocess')
        foundstr = "".join([self.key[l]  for l in inference_output])
        postprocess_output = [foundstr]
        print('shawn-keyresult:', postprocess_output)
        return postprocess_output

    def handle(self, data, context):
        if not self.initialized:
            self.initialized()
        if data is None:
            print('Data Type is None')
            return None
        else:
            print('shawn-handle, data type:', type(data))
            model_input = self.preprocess(data)
            model_output = self.inference(model_input)
            return self.postprocess(model_output) 
