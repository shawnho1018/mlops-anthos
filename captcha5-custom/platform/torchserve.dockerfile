FROM pytorch/torchserve:latest
USER root
RUN apt-get update
RUN pip install --upgrade pip
RUN pip install sklearn scipy
RUN pwd
RUN ls -la
COPY ./sources /home/model-server/
COPY config.properties /home/model-server/
ARG MODELNAME
RUN torch-model-archiver --model-name ${MODELNAME} --version 1.0 --model-file model.py --serialized-file conv_ae_013399.pth --extra-files background_d.pkl,utils.py,img_prepro.py --handler handler_captcha5.py --export-path ./model-store -f
CMD ["torchserve", \
     "--start", \
     "--ts-config=/home/model-server/config.properties", \
     "--model-store", \
     "model-store", \
     "--models", \
     "${MODELNAME}=${MODELNAME}.mar"]
