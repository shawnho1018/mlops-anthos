#!/bin/bash
export MODEL_NAME=captcha5-custom

cat << EOF > config.properties
inference_address=http://0.0.0.0:8080
management_address=http://0.0.0.0:8081
number_of_netty_threads=4
service_envelope=kfserving
job_queue_size=10
model_store=/home/model-server/model-store
model_snapshot={"name":"startup.cfg","modelCount":1,"models":{"${MODEL_NAME}":{"1.0":{"defaultVersion":true,"marName":"${MODEL_NAME}.mar","minWorkers":1,"maxWorkers":5,"batchSize":1,"maxBatchDelay":5000,"responseTimeout":120}}}}
EOF

export DOCKER_BUILDKIT=1
docker build --build-arg MODELNAME=${MODEL_NAME} -t gcr.io/tsmc-demo/${MODEL_NAME}:latest -f ./torchserve.dockerfile .
docker push gcr.io/tsmc-demo/${MODEL_NAME}:latest

cat << EOF > "${MODEL_NAME}.yaml"
apiVersion: "serving.kubeflow.org/v1beta1"
kind: "InferenceService"
metadata:
  name: "${MODEL_NAME}"
spec:
  predictor:
    minReplicas: 1
    containers:
    - name: custom-captcha5
      image: gcr.io/tsmc-demo/${MODEL_NAME}:latest
      ports:
        - containerPort: 8080
          protocol: TCP
EOF

#gsutil cp config.properties gs://tsmc-demo/${MODEL_NAME}/config/
#gsutil cp "${MODEL_NAME}.mar" gs://tsmc-demo/${MODEL_NAME}/model-store/

kubectl apply -f ${MODEL_NAME}.yaml
