#!/bin/bash
export MODEL_NAME=captcha5-custom

cat << EOF > config.properties
inference_address=http://0.0.0.0:8085
management_address=http://0.0.0.0:8081
metrics_address=http://0.0.0.0:8082
number_of_netty_threads=4
job_queue_size=10
model_store=/home/model-server/model-store
EOF

export DOCKER_BUILDKIT=1
docker build --build-arg MODELNAME=${MODEL_NAME} -t gcr.io/tsmc-demo/${MODEL_NAME}:latest -f ./torchserve.dockerfile .
docker push gcr.io/tsmc-demo/${MODEL_NAME}:latest

docker run -d -p 5500:8085 --name serve gcr.io/tsmc-demo/${MODEL_NAME}:latest
