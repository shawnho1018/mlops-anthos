#!/bin/bash
ACTION=$1
if [ -z "${ACTION}" ]; then
    echo "an 'ACTION' is required (deploy || delete || check)"
    exit 1
fi

# Script local - 3-digit suffix
export ITERATION_SUFFIX="101"
export SUFFIX=$ITERATION_SUFFIX
export DEBUG="true"
export FANCY_OK="\e[30;48;5;82m OK \e[0m "
export FANCY_FAIL="\e[30;48;5;1m FAIL \e[0m "
export FANCY_NEUTRAL="\e[30;48;5;248m FAIL \e[0m "
export WORKDIR=$(pwd)
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export ACCOUNT_EMAIL=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")


if [ ! -f "${WORKDIR}/vars-${ITERATION_SUFFIX}.sh" ]; then
  echo "Creating a file with all of the variables for this workshop"
fi

cat <<EOT >> ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
export WORKDIR=$(pwd)
export SUFFIX=${ITERATION_SUFFIX}
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export ACCOUNT_EMAIL=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
EOT

function create_cluster() {
  local CLUSTER_EXISTS=$(gcloud container clusters describe "cluster-${SUFFIX}" --format="value(name)" 2> /dev/null)
  if [ -z "${CLUSTER_EXISTS}" ]; then
    # Cluster does not exist
    echo "Create clusters at $PROJECT_ID Project"
    echo "Creating a new GKE Cluster...(may take 3-10 minutes)"
    gcloud container clusters create "cluster-$SUFFIX" \
        --release-channel rapid \
        --enable-ip-alias \
        --workload-pool=${PROJECT_ID}.svc.id.goog \
        --enable-autoscaling --min-nodes 1 --max-nodes 4 --num-nodes 2 \
        --machine-type=n1-standard-4 \
        --labels mesh_id="proj-${PROJECT_NUMBER}" \
        --scopes=cloud-platform \
        --quiet \
        --zone ${ZONE}
    echo -e "${FANCY_OK} Cluster created, use this command to create your 'kubectl' context configuration:"
  else
    echo -e "${FANCY_NEUTRAL} Cluster already exists, moving on"
  fi

  echo "Re-connect to cluster:  gcloud container clusters get-credentials cluster-${SUFFIX} --zone ${ZONE} --project ${PROJECT_ID}"
  gcloud container clusters get-credentials cluster-${SUFFIX} --zone ${ZONE} --project ${PROJECT_ID}
}

function install_kubeflow() {
  source vars-101.sh
  export CONFIG_FILE=./kfctl_istio_dex-120.yaml
  if [[ ! -f ${CONFIG_FILE} ]]; then
    wget https://raw.githubusercontent.com/kubeflow/manifests/master/kfdef/kfctl_istio_dex.v1.2.0.yaml -O ${CONFIG_FILE}
    #sed '' -i "s|kubeflow\/manifest|DavidSpek\/manifest|g" ${CONFIG_FILE}
  fi
  # Credentials for the default user are admin@kubeflow.org:12341234
  # To change them, please edit the dex-auth application parameters
  # inside the KfDef file.
  #vim $CONFIG_FILE

  kfctl apply -V -f ${CONFIG_FILE}
  kubectl patch -n kubeflow clusterrole kubeflow-pipelines-cache-deployer-clusterrole --patch "$(cat cache-clusterrole-patch.yaml)"
  kubectl patch svc istio-ingressgateway -n istio-system -p '{"spec": {"type": "LoadBalancer", "loadBalancerIP":"104.155.215.124"}}'
}

function install_kfserving() {
  set -e

  export ISTIO_VERSION=1.6.2
  export KNATIVE_VERSION=v0.18.0
  export KFSERVING_VERSION=v0.5.0-rc2
  if [ ! -d istio-${ISTIO_VERSION} ]; then
    curl -L https://git.io/getLatestIstio | sh -
  else
    echo "istio $ISTIO_VERSION has been downloaded...skip"
  fi
  cd istio-${ISTIO_VERSION}

  # Create istio-system namespace
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Namespace
metadata:
  name: istio-system
  labels:
    istio-injection: disabled
EOF

cat << EOF > ./istio-minimal-operator.yaml
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
spec:
  values:
    global:
      proxy:
        autoInject: disabled
      useMCP: false
      # The third-party-jwt is not enabled on all k8s.
      # See: https://istio.io/docs/ops/best-practices/security/#configure-third-party-service-account-tokens
      jwtPolicy: first-party-jwt

  addonComponents:
    pilot:
      enabled: true
    tracing:
      enabled: true
    kiali:
      enabled: true
    prometheus:
      enabled: true
    grafana:
      enabled: true

  components:
    ingressGateways:
      - name: istio-ingressgateway
        enabled: true
      - name: cluster-local-gateway
        enabled: true
        label:
          istio: cluster-local-gateway
          app: cluster-local-gateway
        k8s:
          service:
            type: ClusterIP
            ports:
            - port: 15020
              name: status-port
            - port: 80
              name: http2
            - port: 443
              name: https
EOF

  bin/istioctl manifest apply -f istio-minimal-operator.yaml

  # Install Knative
  kubectl apply --filename https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-crds.yaml
  kubectl apply --filename https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-core.yaml
  kubectl apply --filename https://github.com/knative/net-istio/releases/download/${KNATIVE_VERSION}/release.yaml

  # Install Cert Manager
  kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.1/cert-manager.yaml
  kubectl wait --for=condition=available --timeout=600s deployment/cert-manager-webhook -n cert-manager
  cd ..
  # Install KFServing
  K8S_MINOR=$(kubectl version | perl -ne 'print $1."\n" if /Server Version:.*?Minor:"(\d+)"/')
  if [[ $K8S_MINOR -lt 16 ]]; then
    kubectl apply -f https://raw.githubusercontent.com/kubeflow/kfserving/master/install/${KFSERVING_VERSION}/kfserving.yaml --validate=false
  else
    kubectl apply -f https://raw.githubusercontent.com/kubeflow/kfserving/master/install/${KFSERVING_VERSION}/kfserving.yaml
  fi
  # patch loadbalancerIP
  kubectl patch svc istio-ingressgateway -n istio-system --patch "$(cat patch-file.yaml)"
  # Clean up
  rm -rf istio-${ISTIO_VERSION}
}

function install_asm() {
  if [[ ! -f install_asm ]]; then
    curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_1.8 > install_asm
    chmod +x install_asm
    if [[ -d "asm_install" ]]; then
      rm -rf asm_install
    fi
    mkdir -p asm_install
  fi
  ./install_asm -v --project_id ${PROJECT_ID} \
    --cluster_name "cluster-${SUFFIX}" \
    --cluster_location ${ZONE} \
    --mode install \
    --output_dir asm_install \
    --enable_gcp_iam_roles --enable_cluster_labels
#    --only-validate
}

function delete_kubeflow() {
  source vars-101.sh
  export CONFIG_FILE=./kfctl_istio_dex.yaml
  kfctl delete -V -f ${CONFIG_FILE}
  rm -rf .cache/
  rm -rf kustomize/
}

function delete_asm() {
  istioctl manifest generate --set profile=asm-gcp | kubectl delete --ignore-not-found=true -f -
  kubectl delete ns istio-system
}

if [ ${ACTION} = "deploy" ]; then
  echo "deploy..."
  create_cluster
  install_kfserving
elif [ ${ACTION} = "delete" ]; then
  echo "delete kubeflow..."
  delete_kubeflow
  #delete_asm
else
  echo "No action for it yet"
fi
