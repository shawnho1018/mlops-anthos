# KFP-Keras
This project is a demonstration how to utilize Vertex pipeline. There are 2 tutorials in this folder. 
## pipeline-tutorial.ipynb: 
[Vertex Pipelines: Pipelines introduction for KFP](https://github.com/GoogleCloudPlatform/vertex-ai-samples/blob/master/notebooks/official/pipelines/pipelines_intro_kfp.ipynb)

### Demo: 
We showed how to quickly create vertex pipelines using kfp & kfp.v2. The demos are straightforward. 
1. Hello World with aiplatform job run.
2. Operator computation with aiplatform a scheduled job run. 


## breast-cancer.ipynb: 
[Serverless Machine Learning Pipelines with Vertex AI: An Introduction](https://towardsdatascience.com/serverless-machine-learning-pipelines-with-vertex-ai-an-introduction-30af8b53188e)

### Demo: 
We showed 
1. How to use [load_breast_cancer](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html) to retrieve breast cancer diagnostic statistics. 
2. Use Xgboost to train and eval the breast cancer data. 
3. Calculate ROC curves and confusion matrix.