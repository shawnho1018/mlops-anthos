#!/bin/bash
ENDPOINT_ID="17668887975i56965376"
PROJECT_ID="shawn-demo-2021"
INPUT_DATA_FILE='{
  "instances": [
    { "data": "1.4838871833555929,
 1.8659883497083019,
 2.234620276849616,
 1.0187816540094903,
 -2.530890710602246,
 -1.6046416850441676,
 -0.4651483719733302,
 -0.4952254087173721,
 0.7746763768735953" }
  ]
}'

gcloud auth application-default login

curl \
-X POST \
-H "Authorization: Bearer $(gcloud auth print-access-token)" \
-H "Content-Type: application/json" \
https://asia-east1-aiplatform.googleapis.com/v1/projects/${PROJECT_ID}/locations/asia-east1/endpoints/${ENDPOINT_ID}:predict \
-d "@${INPUT_DATA_FILE}"
