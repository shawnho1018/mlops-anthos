# MLops Study Note 1: Keras based Training
This project adapts the tutorial from [codelab](https://codelabs.developers.google.com/vertex_custom_training_prediction).
The goal of this project is to learn how to create a custom model for keras. 
Step 1: Create Vertex AI Notebook. 
![create image](images/notebook.png)
