#!/bin/bash
export BUCKET=tsmc-demo
export kubeflow_ns=kubeflow

gsutil mb "gs://${BUCKET}"
gcloud iam service-accounts create ai-admin --display-name "AI-admin service account"

SERVICE_ACCOUNT_EMAIL=$(gcloud iam service-accounts list \
  --filter="displayName:AI-admin service account" \
  --format 'value(email)')

gsutil iam ch serviceAccount:$SERVICE_ACCOUNT_EMAIL:objectAdmin gs://${BUCKET}

gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:${BUCKET}.svc.id.goog[kubeflow/pipeline-runner]" \
  ai-admin@tsmc-demo.iam.gserviceaccount.com
  
  
kubectl annotate serviceaccount \
  --namespace ${kubeflow_ns} \
  pipelinerunner \
  iam.gke.io/gcp-service-account="ai-admin@${BUCKET}.iam.gserviceaccount.com"