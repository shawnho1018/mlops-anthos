{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lightweight python components\n",
    "\n",
    "Lightweight python components do not require you to build a new container image for every code change.\n",
    "They're intended to use for fast iteration in notebook environment.\n",
    "\n",
    "#### Building a lightweight python component\n",
    "To build a component just define a stand-alone python function and then call kfp.components.func_to_container_op(func) to convert it to a component that can be used in a pipeline.\n",
    "\n",
    "There are several requirements for the function:\n",
    "* The function should be stand-alone. It should not use any code declared outside of the function definition. Any imports should be added inside the main function. Any helper functions should also be defined inside the main function.\n",
    "* The function can only import packages that are available in the base image. If you need to import a package that's not available you can try to find a container image that already includes the required packages. (As a workaround you can use the module subprocess to run pip install for the required package. There is an example below in my_divmod function.)\n",
    "* If the function operates on numbers, the parameters need to have type hints. Supported types are ```[int, float, bool]```. Everything else is passed as string.\n",
    "* To build a component with multiple output values, use the typing.NamedTuple type hint syntax: ```NamedTuple('MyFunctionOutputs', [('output_name_1', type), ('output_name_2', float)])```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Install the SDK\n",
    "#!pip3 install 'kfp>=0.1.31.2' --quiet"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import kfp\n",
    "import kfp.components as comp\n",
    "import kfp.dsl as dsl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simple function that just add two numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Define a Python function\n",
    "@comp.func_to_container_op\n",
    "def add(a: float, b: float) -> float:\n",
    "   '''Calculates sum of two arguments'''\n",
    "   return a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Convert the function to a pipeline operation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "#add_op = comp.func_to_container_op(add)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A bit more advanced function which demonstrates how to use imports, helper functions and produce multiple outputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Advanced function\n",
    "#Demonstrates imports, helper functions and multiple outputs\n",
    "from typing import NamedTuple\n",
    "def my_divmod(dividend: float, divisor:float) -> NamedTuple('MyDivmodOutput', [('quotient', float), ('remainder', float), ('mlpipeline_ui_metadata', 'UI_metadata'), ('mlpipeline_metrics', 'Metrics')]):\n",
    "    '''Divides two numbers and calculate  the quotient and remainder'''\n",
    "    #Pip installs inside a component function.\n",
    "    #NOTE: installs should be placed right at the beginning to avoid upgrading a package\n",
    "    # after it has already been imported and cached by python\n",
    "    import sys, subprocess;\n",
    "    subprocess.run([sys.executable, '-m', 'pip', 'install', 'tensorflow==1.8.0'])\n",
    "    \n",
    "    #Imports inside a component function:\n",
    "    import numpy as np\n",
    "\n",
    "    #This function demonstrates how to use nested functions inside a component function:\n",
    "    def divmod_helper(dividend, divisor):\n",
    "        return np.divmod(dividend, divisor)\n",
    "\n",
    "    (quotient, remainder) = divmod_helper(dividend, divisor)\n",
    "\n",
    "    from tensorflow.python.lib.io import file_io\n",
    "    import json\n",
    "    \n",
    "    # Exports a sample tensorboard:\n",
    "    metadata = {\n",
    "      'outputs' : [{\n",
    "        'type': 'tensorboard',\n",
    "        'source': 'gs://ml-pipeline-dataset/tensorboard-train',\n",
    "      }]\n",
    "    }\n",
    "\n",
    "    # Exports two sample metrics:\n",
    "    metrics = {\n",
    "      'metrics': [{\n",
    "          'name': 'quotient',\n",
    "          'numberValue':  float(quotient),\n",
    "        },{\n",
    "          'name': 'remainder',\n",
    "          'numberValue':  float(remainder),\n",
    "        }]}\n",
    "\n",
    "    from collections import namedtuple\n",
    "    divmod_output = namedtuple('MyDivmodOutput', ['quotient', 'remainder', 'mlpipeline_ui_metadata', 'mlpipeline_metrics'])\n",
    "    return divmod_output(quotient, remainder, json.dumps(metadata), json.dumps(metrics))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test running the python function directly"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_divmod(100, 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Convert the function to a pipeline operation\n",
    "\n",
    "You can specify an alternative base container image (the image needs to have Python 3.5+ installed)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "divmod_op = comp.func_to_container_op(my_divmod, base_image='tensorflow/tensorflow:1.11.0-py3')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Define the pipeline\n",
    "Pipeline function has to be decorated with the `@dsl.pipeline` decorator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "import kfp.dsl as dsl\n",
    "@dsl.pipeline(\n",
    "   name='Calculation pipeline',\n",
    "   description='A toy pipeline that performs arithmetic calculations.'\n",
    ")\n",
    "def calc_pipeline(\n",
    "   a='a',\n",
    "   b='7',\n",
    "   c='17',\n",
    "):\n",
    "    #Passing pipeline parameter and a constant value as operation arguments\n",
    "    add_task = add(a, 4) #Returns a dsl.ContainerOp class instance. \n",
    "    \n",
    "    #Passing a task output reference as operation arguments\n",
    "    #For an operation with a single return value, the output reference can be accessed using `task.output` or `task.outputs['output_name']` syntax\n",
    "    divmod_task = divmod_op(add_task.output, b)\n",
    "\n",
    "    #For an operation with a multiple return values, the output references can be accessed using `task.outputs['output_name']` syntax\n",
    "    result_task = add(divmod_task.outputs['quotient'], c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Submit the pipeline for execution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "unsupported operand type(s) for divmod(): 'PipelineParam' and 'PipelineParam'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-14-3c7ff0b78578>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      4\u001b[0m \u001b[0;31m#Submit a pipeline run\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      5\u001b[0m \u001b[0mclient\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mkfp\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mClient\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mhost\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;34m'17fb6eade44853a9-dot-asia-east1.pipelines.googleusercontent.com'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 6\u001b[0;31m \u001b[0mclient\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mcreate_run_from_pipeline_func\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mcalc_pipeline\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0marguments\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0marguments\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      7\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      8\u001b[0m \u001b[0;31m# Run the pipeline on a separate Kubeflow Cluster instead\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/.local/lib/python3.7/site-packages/kfp/_client.py\u001b[0m in \u001b[0;36mcreate_run_from_pipeline_func\u001b[0;34m(self, pipeline_func, arguments, run_name, experiment_name, pipeline_conf, namespace)\u001b[0m\n\u001b[1;32m    689\u001b[0m     \u001b[0;32mwith\u001b[0m \u001b[0mtempfile\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mTemporaryDirectory\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;32mas\u001b[0m \u001b[0mtmpdir\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    690\u001b[0m       \u001b[0mpipeline_package_path\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mos\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpath\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mjoin\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mtmpdir\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m'pipeline.yaml'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 691\u001b[0;31m       \u001b[0mcompiler\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mCompiler\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mcompile\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mpipeline_func\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mpipeline_package_path\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mpipeline_conf\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mpipeline_conf\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    692\u001b[0m       \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mcreate_run_from_pipeline_package\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mpipeline_package_path\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0marguments\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mrun_name\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mexperiment_name\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mnamespace\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    693\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/.local/lib/python3.7/site-packages/kfp/compiler/compiler.py\u001b[0m in \u001b[0;36mcompile\u001b[0;34m(self, pipeline_func, package_path, type_check, pipeline_conf)\u001b[0m\n\u001b[1;32m    949\u001b[0m           \u001b[0mpipeline_func\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mpipeline_func\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    950\u001b[0m           \u001b[0mpipeline_conf\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mpipeline_conf\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 951\u001b[0;31m           package_path=package_path)\n\u001b[0m\u001b[1;32m    952\u001b[0m     \u001b[0;32mfinally\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    953\u001b[0m       \u001b[0mkfp\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mTYPE_CHECK\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtype_check_old_value\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/.local/lib/python3.7/site-packages/kfp/compiler/compiler.py\u001b[0m in \u001b[0;36m_create_and_write_workflow\u001b[0;34m(self, pipeline_func, pipeline_name, pipeline_description, params_list, pipeline_conf, package_path)\u001b[0m\n\u001b[1;32m   1003\u001b[0m         \u001b[0mpipeline_description\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1004\u001b[0m         \u001b[0mparams_list\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m-> 1005\u001b[0;31m         pipeline_conf)\n\u001b[0m\u001b[1;32m   1006\u001b[0m     \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_write_workflow\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mworkflow\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mpackage_path\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1007\u001b[0m     \u001b[0m_validate_workflow\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mworkflow\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/.local/lib/python3.7/site-packages/kfp/compiler/compiler.py\u001b[0m in \u001b[0;36m_create_workflow\u001b[0;34m(self, pipeline_func, pipeline_name, pipeline_description, params_list, pipeline_conf)\u001b[0m\n\u001b[1;32m    835\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    836\u001b[0m     \u001b[0;32mwith\u001b[0m \u001b[0mdsl\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mPipeline\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mpipeline_name\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;32mas\u001b[0m \u001b[0mdsl_pipeline\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 837\u001b[0;31m       \u001b[0mpipeline_func\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0margs_list\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    838\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    839\u001b[0m     \u001b[0mpipeline_conf\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mpipeline_conf\u001b[0m \u001b[0;32mor\u001b[0m \u001b[0mdsl_pipeline\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mconf\u001b[0m \u001b[0;31m# Configuration passed to the compiler is overriding. Unfortunately, it's not trivial to detect whether the dsl_pipeline.conf was ever modified.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-13-7e1e820e45c3>\u001b[0m in \u001b[0;36mcalc_pipeline\u001b[0;34m(a, b, c)\u001b[0m\n\u001b[1;32m     14\u001b[0m     \u001b[0;31m#Passing a task output reference as operation arguments\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     15\u001b[0m     \u001b[0;31m#For an operation with a single return value, the output reference can be accessed using `task.output` or `task.outputs['output_name']` syntax\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 16\u001b[0;31m     \u001b[0mdivmod_task\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mdivmod\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0madd_task\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0moutput\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mb\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     17\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     18\u001b[0m     \u001b[0;31m#For an operation with a multiple return values, the output references can be accessed using `task.outputs['output_name']` syntax\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mTypeError\u001b[0m: unsupported operand type(s) for divmod(): 'PipelineParam' and 'PipelineParam'"
     ]
    }
   ],
   "source": [
    "#Specify pipeline argument values\n",
    "arguments = {'a': '1', 'b': '2'}\n",
    "\n",
    "#Submit a pipeline run\n",
    "client = kfp.Client(host='17fb6eade44853a9-dot-asia-east1.pipelines.googleusercontent.com')\n",
    "client.create_run_from_pipeline_func(calc_pipeline, arguments=arguments)\n",
    "\n",
    "# Run the pipeline on a separate Kubeflow Cluster instead\n",
    "# (use if your notebook is not running in Kubeflow - e.x. if using AI Platform Notebooks)\n",
    "# kfp.Client(host='<ADD KFP ENDPOINT HERE>').create_run_from_pipeline_func(calc_pipeline, arguments=arguments)\n",
    "\n",
    "#vvvvvvvvv This link leads to the run information page. (Note: There is a bug in JupyterLab that modifies the URL and makes the link stop working)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "environment": {
   "name": "tf2-gpu.2-3.m61",
   "type": "gcloud",
   "uri": "gcr.io/deeplearning-platform-release/tf2-gpu.2-3:m61"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
