## Data Process is a newbie project to show case how to utilize use pipeline services. 
Jupyter folder: Using Jupyter to write function and execute in the pipeline service. 
1. jupyter/lightweight_component.ipynb: Wrap Add -> DivMod -> Add functions into a static pipeline. Also, it shows how to cascade steps into a pipeline.
2. jupyter/data.ipynb: Test input/output to GCS object. 
    * Edit BUCKET and kubeflow_ns variables. kubeflow_ns implies the namespace where the pipeline was installed.
    * Run the script, which a GCS bucket and the corresponding service-account in kubernetes would be created to access GCS:
    ```
    ./create-bucket-with-wid.sh
    ```
3. jupyter/ai_platform.ipynb: This playbook is an end-to-end ML pipeline, including data downloading, pipeline training, and model inferencing. The dataset utilized Chicago crime data, which could easily be replaced by other dataset. Remember is to bind the following two roles: bigquery.admin and mlengine.admin ,to the service-account, ai-admin. 

    * Execute the playbook accordingly. Once the execution is completed, you should be able to view the execution result from pipeline UI. 
    ![](screenshots/chicago-pipeline.png)

    * Once the pipeline deployed, we could check the deployed model name and version from GCP AI Platform UI, as shown here.
    ![](screenshots/chicago-ai-platform.png)

    * We then could execute the predict cell to get results, shown below.
    ![](screenshots/chicago-predict-result.png)

4. pipeline/reused_pipeline.ipynb: This playbook is to show how to share the model externally without exposing the source code. This sample is an extension of the second data.ipynb. 
    * sources folder: it contains the Dockerfile which prescribed how to package the python code into docker images. The packing process is shown in build.sh. 
    * sum-number.yaml and write-number.yaml: they defined the input/output schemas. Personally, I will think them as the header files in traditional coding, which describe the input/output of the functions/modules. 
    * resued_pipeline.ipynb: this playbook demonstrates how to data scientists could reuse other's "header" files in their development. 
