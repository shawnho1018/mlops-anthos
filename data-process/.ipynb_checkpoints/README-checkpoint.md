## Data Process is a newbie project to show case how to utilize use pipeline services. 
Jupyter folder: Using Jupyter to write function and execute in the pipeline service. 
1. lightweight_component.ipynb: Wrap Add -> DivMod -> Add functions into a static pipeline. Also, it shows how to cascade steps into a pipeline.
2. data.ipynb: Test input/output to GCS object. 
    * Edit BUCKET and kubeflow_ns variables. kubeflow_ns implies the namespace where the pipeline was installed.
    * Run the script, which a GCS bucket and the corresponding service-account in kubernetes would be created to access GCS:
    ```
    ./create-bucket-with-wid.sh
    ```
3. ai_platform.ipynb: This playbook is an end-to-end ML pipeline, including data downloading, pipeline training, and model inferencing. The dataset utilized Chicago crime data, which could easily be replaced by other dataset. The only thing to remember is to bind the following two roles: bigquery.admin and mlengine.admin ,to the service-account, ai-admin. 