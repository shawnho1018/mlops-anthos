import argparse
import os
import random

# Parse agruments.
parser = argparse.ArgumentParser()
parser.add_argument('--count', type=int, required=True, help='a number between 1~100')
parser.add_argument('--output_path', type=str, required=True)
args = parser.parse_args()
count = args.count
output_path = args.output_path
print(output_path)

os.makedirs(os.path.dirname(output_path), exist_ok=True)
with open(output_path, 'w') as writer:
    for i in range(count):
        writer.write(str(i) + '''\n''')