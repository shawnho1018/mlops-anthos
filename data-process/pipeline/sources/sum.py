import argparse, os
# Parse agruments.
parser = argparse.ArgumentParser()
parser.add_argument('--input_path', type=str, required=True, help='Get number file')
parser.add_argument('--output_path', type=str, required=True)
args = parser.parse_args()
input_path = args.input_path
output_path = args.output_path
print("input:" + input_path)
print("output:" + output_path)
sum = 0

with open(input_path, 'r') as reader:
    line = reader.read()
    num_list = line.split('\n')

print(num_list)
for i in range((len(num_list)-1)):
    sum = sum + int(num_list[i])

if not os.path.exists(os.path.dirname(output_path)):
    os.makedirs(os.path.dirname(output_path))

with open(output_path, 'w') as writer:
    writer.write(str(sum) + '''\n''')