# MLops Study Note 1: Keras based Training
This project adapts the tutorial from [codelab](https://codelabs.developers.google.com/vertex_custom_training_prediction).
The goal of this project is to learn how to create a custom model for keras. 

1. Create Vertex AI Notebook:
* Click 『New Notebook』
* Choose TensorFlow Enterprise -> Tensorflow Enterprise 2.3 -> No GPU
![create image](images/notebook.png)

2. Open Terminal in Vertex AI Notebook:

Pull Keras Project
```
# Create a directory, so Git doesn't get messy, and enter it
mkdir vertex-ai && cd vertex-ai

# Start a Git repository
git init

# Track repository, do not enter subdirectory
git remote add -f origin https://gitlab.com/shawnho1018/mlops-anthos

# Enable the tree check feature
git config core.sparseCheckout true

# Create a file in the path: .git/info/sparse-checkout
# That is inside the hidden .git directory that was created
# by running the command: git init
# And inside it enter the name of the sub directory you only want to clone
echo 'keras' >> .git/info/sparse-checkout

## Download with pull, not clone
git pull origin master
```

3. Train Model
