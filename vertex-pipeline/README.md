# Vertex Pipeline with Anthos Integration
This tutorial demonstrates how to utilize Vertex AI along with Cloud Build to create a ready-to-deploy container image. The integration utilizes an open-sourced ML models, BentoML to bridge Vertex AI pipeline along with the Cloudbuild. 

## Xgboost
Inside this folder, there is a breast cancer estimator demonstration. File, whose name begins with 00, indicated this is a working notebook to try out the trainer and inferencer. File, whose name begins with 01, indicated this is a pipeline notebook, which shows how to move a working notebook into a vertex notebook.

Xgboost is a special type of the training model. There are [two different ways](https://blog.csdn.net/phyllisyuell/article/details/81005509) to config and train the xgboost model. 

### Type 1: xgboost
This model was more modern, whose input parameters utilized pandas's DMatrix. To retrieve the model, we normally use train function to get the model. However, if you intended to create an empty one in order to load from saved model, we could utilize the following initial function. Also, unlike Type 2, the prediction output from this xgboost was a probability and a manual bin classification was also required to check the result is positive or negative.
```
import xgboost as xgb

# Train from existing training data
# Need to first convert features and label into DMatrix object for later train function call.
X_y_train = xgb.DMatrix(data=data.drop(["target"], axis=1), label= data["target"])
model = xgb.train(params=params, dtrain=X_y_train, num_boost_round=20)

# Initialize an empty model for model loading
bst = xgb.Booster()
bst.load_model(xgb_model.path)
```

### Type 2: XGBClassifier
The other model was more legacy. It still accepted separate features and label data in numpy. Its training function was called fit and the trained model could be retrieved easily from XGBclassifer object. We could read how it was applied in the following code. Interestingly, the prediction results from this model would automatically be converted to 0 or 1 (without or with breast cancer).
```
import xgboost as xgb
from xgboost import XGBClassifier
import numpy as np

model2 = XGBClassifier(
    base_score= np.mean(train['target']),
    eta=0.1,
    max_depth=3,
    gamma=3,
    n_estimatpr=20,
    seed=27,
    silent=0,
    objective='reg:logistic'
)
model2.fit(
    train.drop(columns=["target"]),
    train.target,4
)

# Initialize an empty model for model loading
model_reload = xgb.XGBClassifier()
model_reload.load_model("./model2.dat")
```

We currently adapted <b>type 1</b> in our pipeline because this seems the only one BentoML supported in its xgboost model. The resulting vertex pipeline could be viewed below:
![xgboost with Vertex Pipeline](images/xgboost-pipeline.png)


## Keras
This folder is created to show how to utilize Vertex AI for Keras model training and inferencing. We intended to demonstrate vehicle's mileages per gallon estimaton from vehicle's feature data. There seems many optimizers for Keras and the one we selected was RMSprop. BentoML's Keras model could adapt the model trained by Vertex pipeline.

One thing to remind us all is that python 3.9 and 3.7 will produce incompatible keras model file; therefore, extra care must be taken when training and reloading the model in different base images. Detailed explanation could refer to the notebook file. 

![Keras with Vertex Pipeline](images/keras-pipeline.png)
