from typing import List

import pandas as pd
import numpy as np
from tensorflow import keras
from tensorflow.keras.preprocessing import text
from bentoml import api, env, BentoService, artifacts
from bentoml.frameworks.keras import KerasModelArtifact
from bentoml.service.artifacts.common import PickleArtifact
from bentoml.adapters import JsonInput

max_features = 1000
    
@env(infer_pip_packages=True)
@artifacts([
    KerasModelArtifact('model'),
    PickleArtifact('stat')
])
class KerasModelService(BentoService):
    def normalize(self, raw):
        return (raw - np.array(self.artifacts.stat.loc['mean']).reshape(1,9))/np.array(self.artifacts.stat.loc['std']).reshape(1,9)
    @api(input=JsonInput(), batch=False)
    def predict(self, parsed_json):
        import numpy as np
        """ //if the input is string [[1.0,2.0,3.0]]..etc, the following is required to parse
        import numpy as np
        data  = text.text_to_word_sequence(parsed_json['text'], filters='\[\] ', split=',')
        array = np.array(data)
        array = array.astype(np.float)
        array = np.reshape(array, (-1, 9))
        """
        data = parsed_json['text']
        norm_data = self.normalize(data)
        return self.artifacts.model.predict(norm_data)
