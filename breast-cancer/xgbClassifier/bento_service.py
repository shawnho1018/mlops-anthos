import pandas as pd
import xgboost as xgb
from bentoml import env, artifacts, api, BentoService
from bentoml.adapters import DataframeInput
from bentoml.frameworks.xgboost import XgboostModelArtifact

@env(infer_pip_packages=True)
@artifacts([XgboostModelArtifact('xgbmodel')])
class xgbClassifier(BentoService):
    """
    A minimum prediction service exposing a Scikit-learn model
    """
    @api(input=DataframeInput(), batch=True)
    def predict(self, df):
        """
        An inference API named `predict` with Dataframe input adapter, which codifies
        how HTTP requests or CSV files are converted to a pandas Dataframe object as the
        inference API function input
        """
        data = xgb.DMatrix(data=df)
        return self.artifacts.xgbmodel.predict(data)
